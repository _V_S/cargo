import random

class Container:

    def __init__(self):
        self.__height = 10
        self.__widgth = 10
        self.__length = 20
        self.listOfBox = []
        self.mass = 0
        self.__maxMass = 100
        self.__volume = 3000
        self.freeVolume = self.volume

    @property
    def height(self):
        return self.__height
    @property
    def widgth(self):
        return self.__widgth
    @property
    def length(self):
        return self.__length
    @property
    def volume(self):
        return self.__volume
    @property
    def maxMass(self):
        return self.__maxMass

    def put(self, box):
        ordered = sorted([box.height, box.widgth, box.length])
        if (ordered[0] > 10) or (ordered[1] > 10):
            box.tooBig=True
            return False
        if (box.mass<self.maxMass-self.mass) and (self.freeVolume>box.volume):
            self.listOfBox.append(box)
            self.freeVolume -= box.volume
            self.mass += box.mass
            return True
        return False

    def fit(self, boxes):
        enought = False
        while not enought:
            enought = True
            for box in boxes:
                if not box.tooBig:
                    if self.put(box):
                        enought = False
                        boxes.remove(box)
        return boxes, self
    @staticmethod
    def output(boxes, container):
        print('Коробки, что поместились->')
        [print(box) for box in container.listOfBox]
        print('Коробки, что не поместились->')
        [print(box) for box in boxes]
        print('Вагон->')
        print(container)

    def __str__(self):
        return f'остаток объёма: {self.freeVolume} из {self.volume}, текущая масса: {self.mass} из {self.maxMass}'

class Box:
    def __init__(self):
        self.__height = random.randint(5, 12)
        self.__widgth = random.randint(5, 12)
        self.__length = random.randint(5, 12)
        self.__mass = random.randint(8, 20)
        self.__volume = self.height * self.widgth * self.length
        self.tooBig = False

    @property
    def height(self):
        return self.__height
    @property
    def widgth(self):
        return self.__widgth
    @property
    def length(self):
        return self.__length
    @property
    def volume(self):
        return self.__volume
    @property
    def mass(self):
        return self.__mass
    def __str__(self):
        return f'Габариты коробки: {self.height}Х{self.widgth}Х{self.length}, объем:{self.volume}, масса:{self.mass}'

def start():
    container = Container()
    boxes = [Box() for i in range(10)]
    container.fit(boxes)
    container.output(boxes, container)

if __name__ == '__main__':
   start()